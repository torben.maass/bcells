# Changelog

# v1.0.3 (05.02.2024)

## Feature

* Added documentation and explanation in one of the examples

# v1.0.2 (04.02.2024)

## Feature

* Changed the directory structure when saving data, plots and segmentation results

* Changed background image for boundary images to be clipped and denoised version instead of only the denoised version

# v1.0.1 (04.02.2024)

## Fix

* Fixed plotting functions which would not work with log scale

* Missing dependencies

* Updated documentation (especially for Windows)

# v1.0.0 (01.02.2024)

* Initial release
