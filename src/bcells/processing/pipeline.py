from os import path, makedirs, listdir
import numpy as np
import pandas as pd
from io import StringIO
from nd2 import imread
from typing import Literal, Callable
from scipy.stats import bootstrap
import matplotlib.pyplot as plt
from skimage import color, segmentation, measure
from sys import stdout
from warnings import warn
import hashlib


from .process import (
    average_and_denoise, clip_and_rescale, subtract_local_bg, local_threshold
)
from ..segmentation.segment import (
    segment, process_segmentation
)
from ..segmentation.extract import (
    seg_properties, local_bg_intensity, traces_minus_bg
)

from .wasserstein import generalized_inverse


def generate_indices(*xi):
    indices = np.meshgrid(*xi)
    return [index.flatten() for index in indices]

def save_files_jointly(filepaths: list[str], output_directory, hash_type: str = 'md5', identifier: str = 'sample_representative', columns: list[str] = ['Cell index', 'Characteristic', 'Value'], include_nd2_filepath: bool = False, nd2_filepaths: list[str] = None):
    '''Merges the csv files found in filepaths. Merges only those containing "Cell index" as column. Assumes the newer non-pivot structure, as currently output by this package in "sample_characteristics" files.'''
    # Validate inputs
    if not all([path.endswith('.csv') for path in filepaths]):
        raise ValueError("All files must be CSV files");
    if not path.exists(output_directory):
        makedirs(output_directory);
    def string_to_md5_int(input_string):
        # Create MD5 hash object
        md5_hash = hashlib.md5(input_string.encode())
        
        # Convert the MD5 hash to an integer
        md5_int = int(md5_hash.hexdigest(), 16)
        
        return md5_int
    # Initialize an empty list to collect all columns and combined data
    
    combined_data = [];

    # Loop through each CSV file in the file paths
    for index, filepath in enumerate(filepaths):
        # Read the CSV file
        try:
            df = pd.read_csv(filepath, index_col = 0);
        except Exception as e:
            raise ValueError(f"Failed to read file {filepath}: {e}");

        # Extract the directory name and filename base (excluding extension and identifier)
        directory = path.dirname(filepath);
        filename_with_extension = path.basename(filepath);
        filename = filename_with_extension.split(f'_{identifier}.csv')[0];  # Strip off identifier part

        # Check if the file contains a cell index (normal file vs barycenter file)
        if set(df.columns) == set(columns):
            # This is a normal file (with cell index)
            df_pivoted = df.pivot(index='Cell index', columns='Characteristic', values='Value');
            df_pivoted.reset_index(inplace=True);
            df_pivoted.columns.name = None;

            if hash_type == "md5":
                directory = string_to_md5_int(directory);
                filename = string_to_md5_int(filename);
            # Add Directory, Filename
            df_pivoted['Directory'] = directory;
            df_pivoted['Filename'] = filename;

            # Reorder columns to ensure 'row_id', 'Directory', and 'Filename' columns come first
            first_columns = ['Directory', 'Filename', 'Cell index'];
            if include_nd2_filepath:
                nd2_filepath_column_name = 'ND2 Filepath';
                first_columns = [nd2_filepath_column_name] + first_columns;
                nd2_filepath = nd2_filepaths[index];
                if hash_type == 'md5':
                    nd2_filepath = string_to_md5_int(nd2_filepath);
                df_pivoted[nd2_filepath_column_name] = nd2_filepath;
            all_columns_pivoted = first_columns + [col for col in df_pivoted.columns if col not in first_columns];
            df_pivoted = df_pivoted[all_columns_pivoted];

            # Append the data to combined_data
            combined_data.append(df_pivoted);


    # Concatenate all the data into a single dataframe
    if combined_data:
        combined_df = pd.concat(combined_data, ignore_index=True);

        # Output file path
        output_filename = path.join(output_directory, f"all_{identifier}");
        if hash_type is not None:
            output_filename += f"_{hash_type}";
        output_filename += ".csv";
        combined_df.to_csv(output_filename, index = True);


    

def save_segmentation_and_extraction_results(results: dict, filename: str, output_directory: str):
    filename_wo_ending, fileending = path.splitext(filename) # filename.split('.')[0]
    ### create and store images from segmentation
    output_directory = path.join(output_directory, 'Segmentation')
    # What data should we store?
    # data:
    #   - seg, seg_props, seg_local_bg_cells, traces_bg
    # images:
    #   - first frame
    #   - denoised
    #   - local background
    #   - local threshold
    #   - segmentation (with colors)
    #   - segmentation (with colors and underlay)
    #   - segmentation (with yellow borders and underlay)
    #   - segmentation (with yellow borders and underlay and numbers)

    makedirs(output_directory, exist_ok = True) # create directory if does not exist

    ### create more directories for storing data
    # directories for data
    # directories for images
    first_frame_dir = path.join(output_directory, 'first_frame')
    denoised_dir = path.join(output_directory, 'denoised')
    est_bg_dir = path.join(output_directory, 'est_bg')
    local_thresh_img_dir = path.join(output_directory, 'local_thresh')
    local_thresh_markers_dir = path.join(output_directory, 'local_thresh_markers')
    seg_color_dir = path.join(output_directory, 'seg_color')
    seg_boundary_dir = path.join(output_directory, 'seg_boundary')
    seg_boundary_labeled_dir = path.join(output_directory, 'seg_boundary_labeled')
    seg_dirs = [first_frame_dir, denoised_dir, est_bg_dir, local_thresh_img_dir, local_thresh_markers_dir, seg_color_dir, seg_boundary_dir, seg_boundary_labeled_dir]
    for d in seg_dirs:
        makedirs(d, exist_ok=True)

    ### store results data
    object_names = ['seg', 'seg_props', 'seg_local_bg_cells', 'tracebundle', 'tracebundle']
    file_endings = ['.npy', '.pkl', '.pkl', '.pkl', '.csv']
    file_names = ['_'.join([filename_wo_ending, obj_name]) + end for obj_name, end in zip(object_names, file_endings)]
    paths = [path.join(d, file_name) for file_name, d in zip(file_names, seg_dirs)]

    np.save(paths[0], results['seg'])
    results['seg_props'].to_pickle(paths[1])
    results['seg_local_bg_cells'].to_pickle(paths[2])
    results['tracebundle'].to_pickle(paths[3])
    results['tracebundle'].to_csv(paths[4])


    ### store images from segmentation process
    imgs_black_and_white = [results['first_frame'], results['denoised'], results['est_bg'], results['local_thresh_img'], results['local_thresh_markers']]
    imgs_file_name = ['first_frame', 'denoised', 'estimated_background', 'local_threshold_1', 'local_threshold_2']
    
    k = 0
    for img, img_file_name in zip(imgs_black_and_white, imgs_file_name):
        # create plot and store plot
        curr_path = path.join(seg_dirs[k], '_'.join([filename_wo_ending, img_file_name]) + '.png')
        k += 1
        fig, ax = plt.subplots(1, 1, figsize=(10, 10))
        ax.imshow(img, cmap='gray')
        ax.axis('off')
        fig.savefig(curr_path, bbox_inches='tight')
        plt.close(fig)

    # plot segmentation results
    seg_color = color.label2rgb(results['seg'], bg_label=0)
    seg_boundary = segmentation.mark_boundaries(results['clipped'], results['seg'])

    curr_path = path.join(seg_dirs[k], '_'.join([filename_wo_ending, 'seg_color']) + '.png')
    k += 1
    fig, ax = plt.subplots(1, 1, figsize=(10, 10))
    ax.imshow(seg_color)
    ax.axis('off')
    fig.savefig(curr_path, bbox_inches='tight')
    plt.close(fig)

    curr_path = path.join(seg_dirs[k], '_'.join([filename_wo_ending, 'seg_boundary']) + '.png')
    k += 1
    fig, ax = plt.subplots(1, 1, figsize=(10, 10))
    ax.imshow(seg_boundary)
    ax.axis('off')
    fig.savefig(curr_path, bbox_inches='tight')
    plt.close(fig)

    curr_path = path.join(seg_dirs[k], '_'.join([filename_wo_ending, 'seg_boundary_labeled']) + '.png')
    k += 1
    fig, ax = plt.subplots(figsize=(10, 10))
    ax.imshow(seg_boundary)
    for region in measure.regionprops(results['seg']):
        ax.annotate(region.label, (max(region.centroid[1]-15, 0), region.centroid[0]), fontsize=6.5, c="yellow")
    ax.axis('off')
    fig.savefig(curr_path, bbox_inches='tight')
    plt.close(fig)
    return paths[3]

def standardize_tracebundle(tracebundle: np.ndarray, fps: int, light_on: int):
    return tracebundle / tracebundle[:, :(fps * light_on), np.newaxis].sum(axis = 1)

def standardize_trace(trace: np.ndarray, fps: int, light_on: int):
    return trace / trace[:(fps * light_on)].sum()

# set file name
def segment_and_extract(filepath: str, verbosity: Literal['silent', 'minimal', 'complete'], output_stream: StringIO):
    """
    Parameters
    ----------
    filepath : str
        Path to the nd2 file to be segmented.
    verbose : bool
        True if progress text should be printed
    output_stream : StringIO
        output stream to write progress text to

    Returns
    -------
    segmentation results : dict

    Notes
    -----
    Multichannel inputs are scaled with all channel data combined.

    Examples
    --------
    """

    verbosity = 2 if verbosity == 'complete' else (1 if verbosity == 'minimal' else (0 if verbosity == 'silent' else verbosity))

    _, filename = path.split(filepath)
    message_prefix = filename + ': '

    # import nd2 file and prepare for segmentation
    if verbosity in [1,2]:
        message = 'Reading images...'
        print(message, file = output_stream)
    image_sequence = imread(filepath)

    # average images over time and denoise
    if verbosity == 2:
        message = 'Denoising images...'
        print(message, file = output_stream)
    denoised, _ = average_and_denoise(image_sequence, frames = slice(120, 280))

    # could start subprocess here, which already reorders `imgs`? Does this 
    # work and actually save time?

    # clip and rescale image
    if verbosity == 2:
        message = 'Clipping and rescaling images...'
        print(message, file = output_stream)
    clipped = clip_and_rescale(denoised)
    # subtract local background by local thresholding (via local what? mean or gaussian?)
    if verbosity == 2:
        message = 'Subtracting local background...'
        print(message, file = output_stream)
    c_minus_bg, est_bg = subtract_local_bg(clipped, offset = -0.01)
    # local threshold image
    local_thresh_img, local_thresh_markers, local_img, local_marker, low = local_threshold(c_minus_bg)
    del local_img, local_marker, low
    
    # segement this thresholded image
    if verbosity == 1:
        message = 'Segmenting images...'
        print(message, file = output_stream)
    seg_out = segment(img = local_thresh_img, markers = local_thresh_markers)
    # process segmentation
    seg = process_segmentation(seg_out)
    del seg_out
    # calculate cell segmentation properties
    image_sequence = np.transpose(image_sequence, (1, 2, 0)) # regionprops_table expects last dimension to be time dimension
    seg_props = seg_properties(seg_lbl = seg, imgs = image_sequence)
    # calculate local background intensity
    seg_local_bg_cells = local_bg_intensity(imgs = image_sequence, 
                                            seg_bg = (seg == 0).astype(np.int8), 
                                            cells_slice = seg_props['slice'])
    # subtract background traces from cell traces
    
    if verbosity == 2:
        message = 'Extracting traces...'
        print(message, file = output_stream)
    tracebundle = traces_minus_bg(seg_props, seg_local_bg_cells)

    # store first frame for output before freiing memory by deleting `imgs`
    first_frame = image_sequence[:, :, 0]
    del image_sequence

    return {
                   'seg_props': seg_props, 'seg_local_bg_cells': seg_local_bg_cells, 
                 'tracebundle': tracebundle,      'first_frame': first_frame, 
                    'denoised': denoised,              'est_bg': est_bg, 
            'local_thresh_img': local_thresh_img,         'seg': seg,  
        'local_thresh_markers': local_thresh_markers, 'clipped': clipped
        }

def segmentation_pipeline(filepaths: list[str], output_directory, output_stream: StringIO = stdout, verbosity: Literal['silent', 'minimal', 'complete'] = 'complete', fps: int = 2):
    """
    Pipeline for nd2 image sequence segmentation and trace extraction. Saves the segmentation data and the extracted traces to a given output directory.

    Arguments:
    filepaths: list[str] - list of filepaths to the nd2 files to be segmented
    verbosity: Literal['silent', 'minimal', 'complete'] - determines how many progress messages are printed to the output_stream
    output_directory: str - directory to which the segmentation data is saved
    output_stream: StringIO - Stream to which progress messages are printed. Default: stdout
    fps: int - Frames per second of the nd2 files. This is not important for the segmentation and extraction. Only shows up as a column in the extracted tracebundles. Default: 2
    Returns:
    output_filepaths - list of filepaths where the extracted tracebundles are saved to
    tracebundles - list of extracted tracebundles.
    """
    num_files = len(filepaths)
    verbosity = 2 if verbosity == 'complete' else (1 if verbosity == 'minimal' else (0 if verbosity == 'silent' else verbosity))
    output_filepaths = []
    output_directory_traces = path.join(output_directory, 'Raw traces')
    makedirs(output_directory_traces, exist_ok = True)
    tracebundles = []
    for index, filepath in enumerate(filepaths):
        filedirectory, filename = path.split(filepath)
        if verbosity >= 1:
            message = 'Segmenting file {} of {}: {}'.format(index + 1, num_files, filename)
            print(message, file = output_stream)
        filename_ = path.splitext(filename)[0]

        segmentation_result = segment_and_extract(filepath = filepath, verbosity = verbosity, output_stream = output_stream)
        save_segmentation_and_extraction_results(segmentation_result, filename, output_directory)
        tracebundle: np.ndarray = segmentation_result['tracebundle'].to_numpy()
        areas = segmentation_result['seg_props']['area'].to_numpy()
        m, n = tracebundle.shape
        time_index = np.arange(0, n)
        cell_index = np.arange(0, m)
        indices = generate_indices(cell_index, time_index)
        time = indices[1] / fps
        columns = ['Cell index', 'Time index', 'Time (s)', 'Mean intensity', 'Cell area']
        tracebundles.append(pd.DataFrame(list(zip(*indices, time, tracebundle.T.flatten(), areas[indices[0]])), columns = columns))
        output_filepaths.append(path.join(output_directory_traces, filename_ + '.pkl'))
        tracebundles[-1].to_pickle(path.join(output_directory_traces, filename_ + '.pkl'))
        tracebundles[-1].to_csv(path.join(output_directory_traces, filename_ + '.csv'))
    return output_filepaths, tracebundles

def tracebundle_to_quantiles(tracebundle: np.ndarray):
    density_curves = tracebundle / tracebundle[:, :, np.newaxis].sum(axis = 1)
    cdfs = np.cumsum(density_curves, axis = 1)
    return np.apply_along_axis(generalized_inverse, 1, cdfs)

def barycenter_from_quantiles(quantilebundle, mode: Literal['l1', 'l2']):
    match mode:
        case 'l1':
            quantile = np.median(quantilebundle, axis = 0)
        case 'l2':
            quantile = np.mean(quantilebundle, axis = 0)
    cdf = generalized_inverse(quantile)
    pdf = np.gradient(cdf)
    return pdf / cdf[-1]

def characteristics_from_quantiles(quantilebundle, characteristics, mode: Literal['l1', 'l2'], indices = None):
    m, n = quantilebundle.shape
    if indices is None:
        indices = np.arange(0, m)
    pdf = barycenter_from_quantiles(quantilebundle = quantilebundle[indices], mode = mode)
    
    trace = standardize_trace(pdf, fps = 2, light_on = 60)
    return np.concatenate([np.array([characteristic(trace) for characteristic in characteristics]), trace])

def characteristics_whole_well(tracebundle_times_area, characteristics, indices = None):
    m, n = tracebundle_times_area.shape
    if indices is None:
        indices = np.arange(0, m)
    trace = standardize_trace(tracebundle_times_area[indices].sum(axis = 0), fps = 2, light_on = 60)
    return np.concatenate([np.array([characteristic(trace) for characteristic in characteristics]), trace])

def save_processing_results(processing_results, characteristics, filenames, output_directory, alpha, fps, nd2_filepaths = None):
    processing_folder = 'processing'
    output_directory = path.join(output_directory, processing_folder)
    makedirs(output_directory, exist_ok = True)
    num_characteristics = len(characteristics)
    characteristic_names = [characteristic.__name__ for characteristic in characteristics]
    #Sample characteristics
    for result, filename in zip(processing_results, filenames):
        data = result['sample_characteristics']
        indices = generate_indices(np.arange(len(data)), characteristic_names)
        columns = ['Cell index', 'Characteristic', 'Value']
        df = pd.DataFrame(list(zip(*indices, data.T.flatten())), columns = columns)
        filepath = path.join(output_directory, filename + '_sample_characteristics')
        df.to_csv(filepath + '.csv')
        df.to_pickle(filepath + '.pkl')

    #Representative characteristics
    for method in ['whole_well', 'l1_barycenter', 'l2_barycenter']:
        data = []
        temp_bool = False
        for result, filename in zip(processing_results, filenames):
            if 'sample_characteristics_' + method in result:
                temp_bool = True
                data.append(result['sample_characteristics_' + method][:num_characteristics])
        if temp_bool:
            data = np.array(data)
            indices = generate_indices(filenames, characteristic_names)
            columns = ['Filename', 'Characteristic', 'Value']
            df = pd.DataFrame(list(zip(*indices, data.T.flatten())), columns = columns)
            filepath = path.join(output_directory, method + '_sample_characteristics')
            df.to_csv(filepath + '.csv')
            df.to_pickle(filepath + '.pkl')
    #Representative bundle
    for method in ['whole_well', 'l1_barycenter', 'l2_barycenter']:
        data = []
        temp_bool = False
        for result, filename in zip(processing_results, filenames):
            if 'sample_characteristics_' + method in result:
                temp_bool = True
                data.append(result['sample_characteristics_' + method][num_characteristics:])
        if temp_bool:
            data = np.array(data)
            time_index = np.arange(0, data.shape[1])
            indices = generate_indices(filenames, time_index)
            time = indices[1] / fps
            columns = ['Filename', 'Time index', 'Time (s)', 'Mean intensity']
            df = pd.DataFrame(list(zip(*indices, time, data.T.flatten())), columns = columns)
            filepath = path.join(output_directory, 'traces_' + method)
            df.to_csv(filepath + '.csv')
            df.to_pickle(filepath + '.pkl')
    

    for method in ['whole_well', 'l1_barycenter', 'l2_barycenter']:
        for result, filename in zip(processing_results, filenames):
            if 'bootstrap_results_' + method in result:
                #Bootstrap representatives bundle
                data = result['bootstrap_results_' + method].bootstrap_distribution[num_characteristics:].T
                indices = generate_indices(np.arange(0, data.shape[0]), np.arange(0, data.shape[1]))
                time = indices[1] / fps
                columns = ['Bootstrap replicate index', 'Time index', 'Time (s)', 'Mean intensity']
                df = pd.DataFrame(list(zip(*indices, time, data.T.flatten())), columns = columns)
                filepath = path.join(output_directory, filename + '_' + method + '_bootstrap_tracebundle')
                df.to_csv(filepath + '.csv')
                df.to_pickle(filepath + '.pkl')
                #Bootstrap representatives characteristics
                data = result['bootstrap_results_' + method].bootstrap_distribution[:num_characteristics].T
                indices = generate_indices(np.arange(0, data.shape[0]), characteristic_names)
                columns = ['Bootstrap replicate index', 'Characteristic', 'Value']
                df =  pd.DataFrame(list(zip(*indices, data.T.flatten())), columns = columns)
                filepath = path.join(output_directory, filename + '_' + method + '_bootstrap_characteristics')
                df.to_csv(filepath + '.csv')
                df.to_pickle(filepath + '.pkl')

    #Bootstrap characteristics confidence intervals
    for method in ['whole_well', 'l1_barycenter', 'l2_barycenter']:
        data = []
        temp_bool = False
        for result, filename in zip(processing_results, filenames):
            if 'bootstrap_results_' + method in result:
                temp_bool = True
                data.append(result['bootstrap_results_' + method].confidence_interval.low[:num_characteristics])
                data.append(result['bootstrap_results_' + method].confidence_interval.high[:num_characteristics])
        if temp_bool:
            data = np.array(data)

            columns = ['Filename', 'Characteristic', 'Lower bound', 'Upper bound']
            indices = generate_indices(filenames, characteristic_names)
            lower = data[::2]
            upper = data[1::2]
            df = pd.DataFrame(list(zip(*indices, lower.T.flatten(), upper.T.flatten())), columns = columns)
            filepath = path.join(output_directory, method + '_bootstrap_characteristics_confidence_intervals_{}'.format(alpha))
            data = []
            temp_bool = False
            for result, filename in zip(processing_results, filenames):
                if 'sample_characteristics_' + method in result:
                    temp_bool = True
                    data.append(result['sample_characteristics_' + method][:num_characteristics])
            if temp_bool:
                data = np.array(data)
                df['Sample representative value'] = data.T.flatten()
            df.to_csv(filepath + '.csv')
            df.to_pickle(filepath + '.pkl')

    #Bootstrap representatives confidence intervals
    for method in ['whole_well', 'l1_barycenter', 'l2_barycenter']:
        data = []
        temp_bool = False
        for result, filename in zip(processing_results, filenames):
            if 'bootstrap_results_' + method in result:
                temp_bool = True
                data.append(result['bootstrap_results_' + method].confidence_interval.low[num_characteristics:])
                data.append(result['bootstrap_results_' + method].confidence_interval.high[num_characteristics:])
        if temp_bool:
            data = np.array(data)
            lower = data[::2]
            upper = data[1::2]
            columns = ['Filename', 'Time index', 'Time (s)', 'Lower bound', 'Upper bound']

            time_index = np.arange(0, data.shape[1])
            indices = generate_indices(filenames, time_index)
            time = indices[1] / fps
            
            df = pd.DataFrame(list(zip(*indices, time, lower.T.flatten(), upper.T.flatten())), columns = columns)
            filepath = path.join(output_directory, method + '_bootstrap_tracebundle_confidence_intervals_{}'.format(alpha))
            data = []
            temp_bool = False
            for result, filename in zip(processing_results, filenames):
                if 'sample_characteristics_' + method in result:
                    temp_bool = True
                    data.append(result['sample_characteristics_' + method][num_characteristics:])
            if temp_bool:
                data = np.array(data)
                df['Sample representative mean intensity'] = data.T.flatten()
            
            df.to_csv(filepath + '.csv')
            df.to_pickle(filepath + '.pkl')
    
    csv_filepaths = [];
    for filename in listdir(output_directory):
        if filename.endswith(".csv") and "_sample_characteristics" in filename and not "all_sample_characteristics" in filename:
            filepath = path.join(output_directory, filename)
            csv_filepaths.append(filepath)
    save_files_jointly(filepaths = csv_filepaths, hash_type = None, output_directory = output_directory, identifier = 'sample_characteristics', columns = ['Cell index', 'Characteristic', 'Value'], include_nd2_filepaths = nd2_filepaths is not None, nd2_filepaths = nd2_filepaths);
    save_files_jointly(filepaths = csv_filepaths, hash_type = 'md5', output_directory = output_directory, identifier = 'sample_characteristics', columns = ['Cell index', 'Characteristic', 'Value'], include_nd2_filepaths = nd2_filepaths is not None, nd2_filepaths = nd2_filepaths);
    ### save bootstrapped characteristics for all files in one per method

def process_tracebundle(tracebundle: np.ndarray, areas: np.ndarray, characteristics: list[Callable], verbosity: Literal['silent', 'minimal', 'complete'], output_directory, output_stream: StringIO, whole_well: bool, l1_barycenter: bool, l2_barycenter: bool, bootstrap_method: None | Literal['BCa', 'percentile', 'basic'] = 'percentile', bootstrap_replicates = 100, alpha = 0.05, light_on: int = 60, fps: int = 2):
    verbosity = 2 if verbosity == 'complete' else (1 if verbosity == 'minimal' else (0 if verbosity == 'silent' else verbosity))
    m, n = tracebundle.shape
    indices = np.arange(0, m)
    quantilebundle = tracebundle_to_quantiles(tracebundle = tracebundle)
    tracebundle_st = standardize_tracebundle(tracebundle = tracebundle, fps = fps, light_on = light_on)
    results = {}
    results['sample_characteristics'] = np.array([[characteristic(trace) for characteristic in characteristics] for trace in tracebundle])

    if whole_well:
        if verbosity == 2:
            message = 'Calculating characteristics for \'whole well approach\''
            print(message, file = output_stream)
        current_setting = 'whole_well'
        current_prefix = 'sample_characteristics'
        tracebundle_times_area = tracebundle * areas[:, np.newaxis]
        current_key = '{}_{}'.format(current_prefix ,current_setting)
        results[current_key] = characteristics_whole_well(tracebundle_times_area, characteristics)
        if bootstrap_method is not None:
            current_prefix = 'bootstrap_results'
            current_key = '{}_{}'.format(current_prefix ,current_setting)
            results[current_key] = bootstrap((indices,), lambda x: characteristics_whole_well(tracebundle_times_area, characteristics, x), n_resamples = bootstrap_replicates, method = bootstrap_method, confidence_level = 1 - alpha)
    if l1_barycenter:
        if verbosity == 2:
            message = 'Calculating characteristics for \'l1 barycenter\''
            print(message, file = output_stream)
        current_setting = 'l1_barycenter'
        current_prefix = 'sample_characteristics'
        current_key = '{}_{}'.format(current_prefix ,current_setting)
        results[current_key] = characteristics_from_quantiles(quantilebundle = quantilebundle, characteristics = characteristics, mode = 'l1')
        results['l1_barycenter'] = barycenter_from_quantiles(quantilebundle = quantilebundle, mode = 'l1')
        if bootstrap_method is not None:
            current_prefix = 'bootstrap_results'
            current_key = '{}_{}'.format(current_prefix ,current_setting)
            results[current_key] = bootstrap((indices,), lambda x: characteristics_from_quantiles(quantilebundle, characteristics, 'l1', x), n_resamples = bootstrap_replicates, method = bootstrap_method, confidence_level = 1 - alpha)
    
    if l2_barycenter:
        if verbosity == 2:
            message = 'Calculating characteristics for \'l2 barycenter\''
            print(message, file = output_stream)
        current_setting = 'l2_barycenter'
        current_prefix = 'sample_characteristics'
        current_key = '{}_{}'.format(current_prefix ,current_setting)
        results[current_key] = characteristics_from_quantiles(quantilebundle = quantilebundle, characteristics = characteristics, mode = 'l2')
        results['l2_barycenter'] = barycenter_from_quantiles(quantilebundle = quantilebundle, mode = 'l2')
        if bootstrap_method is not None:
            current_prefix = 'bootstrap_results'
            current_key = '{}_{}'.format(current_prefix ,current_setting)
            results[current_key] = bootstrap((indices,), lambda x: characteristics_from_quantiles(quantilebundle, characteristics, 'l2', x), n_resamples = bootstrap_replicates, method = bootstrap_method, confidence_level = 1 - alpha)
    return results

def _processing_pipeline_from_tracebundles(tracebundles, filenames, characteristics, verbosity, output_directory, output_stream, whole_well, l1_barycenter, l2_barycenter, bootstrap_method, bootstrap_replicates, alpha, light_on, fps, nd2_filepaths = None):
    verbosity = 2 if verbosity == 'complete' else (1 if verbosity == 'minimal' else (0 if verbosity == 'silent' else verbosity))
    num_tracebundles = len(tracebundles)
    #tracebundles_ = []
    for tracebundle, filename in zip(tracebundles, filenames):
        if 'Time index' not in tracebundle.columns:
            raise ValueError(f'Tracebundle {filename} does not contain column \'Time index\'. Cannot proceed.')
        if 'Cell index' not in tracebundle.columns:
            raise ValueError(f'Tracebundle {filename} does not contain column \'Cell index\'. Cannot proceed.')
        if 'Mean intensity' not in tracebundle.columns:
            raise ValueError(f'Tracebundle {filename} does not contain column \'Mean intensity\'. Cannot proceed.')
        if 'Cell area' not in tracebundle.columns:
            warn(f'Tracebundle {filename} does not contain a column \'Cell area\', filling in 1s')
            tracebundle['Cell area'] = [1.0] * tracebundle.shape[0]
    tracebundles_ = [tracebundle[['Cell index', 'Time index', 'Mean intensity']].pivot(index = 'Cell index', columns = 'Time index', values = 'Mean intensity').to_numpy() for tracebundle in tracebundles]
    areas = [tracebundle[['Cell index', 'Time index', 'Cell area']].pivot(index = 'Cell index', columns = 'Time index', values = 'Cell area').to_numpy()[:, 0] for tracebundle in tracebundles]
    tracebundle_lengths = np.array([tracebundle.shape[1] for tracebundle in tracebundles_])
    n = tracebundle_lengths.min()
    if not all(tracebundle_lengths == n):
        warn(f'Inhomogeneous length (frames) of tracebundles. Truncating to shortest value ({n}).')
        tracebundles_ = [tracebundle[:, :n] for tracebundle in tracebundles_]
    processing_results = []
    for index, (filename, tracebundle, area) in enumerate(zip(filenames, tracebundles_, areas)):
        if verbosity >= 1:
            message = 'Processing file {} of {}: {}'.format(index + 1, num_tracebundles, filename)
            print(message, file = output_stream)
        processing_results.append(process_tracebundle(tracebundle = tracebundle, areas = area, characteristics = characteristics, verbosity = verbosity, output_directory = output_directory, output_stream = output_stream, whole_well = whole_well, l1_barycenter = l1_barycenter, l2_barycenter = l2_barycenter, bootstrap_method = bootstrap_method, bootstrap_replicates = bootstrap_replicates, alpha = alpha, light_on = light_on, fps = fps))
    save_processing_results(processing_results = processing_results, characteristics = characteristics, filenames = filenames, output_directory = output_directory, alpha = alpha, fps = fps, nd2_filepaths = nd2_filepaths)
    return processing_results

def _processing_pipeline_from_filepaths(filepaths, characteristics, verbosity, output_directory, output_stream, whole_well, l1_barycenter, l2_barycenter, bootstrap_method, bootstrap_replicates, alpha, light_on, fps, nd2_filepaths = None):
    tracebundles = []
    filenames = []
    for filepath in filepaths:
        filepath_no_ending, fileending = path.splitext(filepath)
        _, filename = path.split(filepath_no_ending)
        filenames.append(filename)
        match fileending:
            case '.csv':
                tracebundles.append(pd.read_csv(filepath, index_col = [0]))
            case '.pkl':
                tracebundles.append(pd.read_pickle(filepath))
            case _:
                raise ValueError(f'Tracebundle {filepath} needs to be a \'csv\' or a \'pkl\' file. Cannot proceed.')
    return _processing_pipeline_from_tracebundles(tracebundles = tracebundles, filenames = filenames, characteristics = characteristics, verbosity = verbosity, output_directory = output_directory, output_stream = output_stream, whole_well = whole_well, l1_barycenter = l1_barycenter, l2_barycenter = l2_barycenter, bootstrap_method = bootstrap_method, bootstrap_replicates = bootstrap_replicates, alpha = alpha, light_on = light_on, fps = fps, nd2_filepaths = nd2_filepaths)

def processing_pipeline(filepaths: list[str] = None, tracebundles: list[pd.DataFrame] = None, 
                        filenames: list[str] = None, characteristics: list[Callable] = None, 
                        verbosity: Literal['silent', 'minimal', 'complete'] = 'minimal', 
                        output_directory = '', output_stream: StringIO = stdout, 
                        whole_well: bool = True, l1_barycenter: bool = True, l2_barycenter: bool = True, 
                        bootstrap_method: None | Literal['BCa', 'percentile', 'basic'] = 'percentile', 
                        bootstrap_replicates = 100, alpha = 0.05, light_on: float = 60, fps: int = 2, nd2_filepaths: list[str] = None):
    """
    Pipeline for nd2 image sequence segmentation and trace extraction. Saves the segmentation data and the extracted traces to a given output directory.
    Can either use paths to the tracebundles or the tracebundles themselves if they are already in loaded, for instance as the return value of the segmentation_pipeline.
    If both are supplied, tracebundles and filenames are ignored in favor of filepaths.
    If the tracebundles are used as input directly, a list of names has to supplied as well.
    In any case, a tracebundle is a table containing as columns:
    "Time index", "Cell index", "Mean intensity" and optionally "Cell area"
    If "Cell area" is not supplied and 'whole_well' is set to True, "Cell area" defaults to 1.0.

    Arguments:
    filepaths: list[str] - list of filepaths to the pkl or csv files of the tracebundles to be processed
    tracebundles: list[pd.DataFrame] - list of dataframes of the tracebundles to be processed
    filenames: list[str] - list of filenames to be used if filepaths argument is not supplied. If not set, defaults to 'File_1', 'File_2', ...
    characteristics: list[Callable] - list of functions to extract curve features. Should take one argument a single trace
    verbosity: Literal['silent', 'minimal', 'complete'] - determines how many progress messages are printed to the output_stream
    output_directory: str - directory to which the processing data is saved
    output_stream: StringIO - Stream to which progress messages are printed. Default: stdout
    whole_well: bool - whether the whole well approach should be calculated
    l1_barycenter: bool - whether the l1 barycenter should be calculated
    l2_barycenter: bool - whether the l2 barycenter should be calculated
    bootstrap_method: Literal['BCa', 'percentile', 'basic'] - which bootstrap method should be used. Default: 'BCa'.
    bootstrap_replicates: int - how many bootstrap replicates should be used. Default: 1000
    alpha: float - the confidence level for the bootstrap confidence intervals. Default: 0.05
    light_on: int - the time after which the light is turned on. Default: 60
    fps: int - Frames per second of the traces. Default: 2
    Returns:
    processing_results: list[dict] - list of processing results.
    """
    if filepaths is not None:
        return _processing_pipeline_from_filepaths(filepaths = filepaths, characteristics = characteristics, verbosity = verbosity, output_directory = output_directory, output_stream = output_stream, whole_well = whole_well, l1_barycenter = l1_barycenter, l2_barycenter = l2_barycenter, bootstrap_method = bootstrap_method, bootstrap_replicates = bootstrap_replicates, alpha = alpha, light_on = light_on, fps = fps, nd2_filepaths = nd2_filepaths)
    elif tracebundles is not None:
        warn(f'If tracebundles argument is used, filenames need to be supplied. Default to \'file_1\', \'file_2\', ..., \'file_{len(tracebundles)}\'')
        if filenames is None:
            filenames = [f'file_{k+1}' for k in range(len(tracebundles))]
        return _processing_pipeline_from_tracebundles(tracebundles = tracebundles, filenames = filenames, characteristics = characteristics, verbosity = verbosity, output_directory = output_directory, output_stream = output_stream, whole_well = whole_well, l1_barycenter = l1_barycenter, l2_barycenter = l2_barycenter, bootstrap_method = bootstrap_method, bootstrap_replicates = bootstrap_replicates, alpha = alpha, light_on = light_on, fps = fps, nd2_filepaths = nd2_filepaths)
    else:
        raise ValueError('Either filepaths or tracebundles has to be supplied')

def segmentation_and_processing_pipeline(filepaths: list[str], characteristics: list[Callable], output_directory: str,  verbosity: Literal['silent', 'minimal', 'complete'] = 'complete',
                                         whole_well: bool = True, l1_barycenter: bool = True, l2_barycenter: bool = True, output_stream: StringIO = stdout,
                                         bootstrap_method: None | Literal['BCa', 'percentile', 'basic'] = 'percentile', bootstrap_replicates: int = 100, alpha: float = 0.05, light_on: int = 60, fps: int = 2):
    """
    Pipeline for nd2 image sequence segmentation and trace extraction and tracebundle processing. Saves the segmentation data, the extracted traces and the processed data to a given output directory.

    Arguments:
    filepaths: list[str] - list of filepaths to the nd2 files to be segmented
    characteristics: list[Callable] - list of functions to extract curve features. Should take one argument a single trace
    verbosity: Literal['silent', 'minimal', 'complete'] - determines how many progress messages are printed to the output_stream
    output_directory: str - directory to which the processing data is saved
    output_stream: StringIO - Stream to which progress messages are printed. Default: stdout
    whole_well: bool - whether the whole well approach should be calculated
    l1_barycenter: bool - whether the l1 barycenter should be calculated
    l2_barycenter: bool - whether the l2 barycenter should be calculated
    bootstrap_method: Literal['BCa', 'percentile', 'basic'] - which bootstrap method should be used. Default: 'BCa'.
    bootstrap_replicates: int - how many bootstrap replicates should be used. Default: 1000
    alpha: float - the confidence level for the bootstrap confidence intervals. Default: 0.05
    light_on: int - the time after which the light is turned on. Default: 60
    fps: int - Frames per second of the traces. Default: 2
    Returns:
    processing_results: list[dict] - list of processing results.
    """
    tb_filepaths, tracebundles = segmentation_pipeline(filepaths = filepaths, verbosity = verbosity, output_directory = output_directory, output_stream = output_stream, fps = fps)
    processing_results = processing_pipeline(filepaths = tb_filepaths, characteristics = characteristics, verbosity = verbosity, output_directory = output_directory, output_stream = output_stream, whole_well = whole_well, l1_barycenter = l1_barycenter, l2_barycenter = l2_barycenter, bootstrap_method = bootstrap_method, bootstrap_replicates = bootstrap_replicates, alpha = alpha, light_on = light_on, fps = fps, nd2_filepaths = filepaths)
    return tracebundles, processing_results
