#################
#### Imports ####

### Standard library imports
from os import path
import sys

### Third party imports
from PyQt5.QtWidgets import (QMainWindow, QFileDialog, QApplication, QVBoxLayout, QToolButton, QListWidgetItem,
     QGridLayout, QAbstractItemView, QWidget, QListWidget, QPushButton)
from PyQt5.QtGui import (QPixmap, QIcon)
from PyQt5.QtCore import (QEvent, Qt)

### Local application imports
from .settings import settings
from .resources import resources
from .constants import *



class MainWidget(QMainWindow):
    def __init__(self):
        super().__init__()
        QApplication.instance().installEventFilter(self)
        self.setAcceptDrops(True)
        title = resources.get_text(_APPLICATION_NAME)
        resolution = settings.get(_RESOLUTION)
        position = settings.get(_POSITION)
        x, y = position.split('+')
        x_size, y_size = resolution.split('x')
        self.setGeometry(int(x), int(y), int(x_size), int(y_size))
        self.setWindowTitle(title)
        self.central_widget = QWidget()
        self.add_remove_files_buttons_layout = QGridLayout()
        self.submit_button_layout = QGridLayout()
        self.submit_button_spacer = QWidget()
        self.submit_button_frame = QWidget()
        self.submit_button_frame.setLayout(self.submit_button_layout)
        self.submit_button = QPushButton()
        self.submit_button_layout.addWidget(self.submit_button_spacer, 0, 0)
        self.submit_button_layout.addWidget(self.submit_button, 0, 1)
        self.submit_button_layout.setColumnStretch(0, 1)
        self.add_remove_files_buttons_spacer = QWidget()
        self.central_layout = QVBoxLayout()
        self.add_remove_files_buttons_frame = QWidget()
        self.add_remove_files_buttons_frame.setLayout(self.add_remove_files_buttons_layout)
        self.list_box = QListWidget()
        self.add_files_button = QToolButton()
        self.remove_files_button = QToolButton()
        self.add_remove_files_buttons_layout.addWidget(self.add_files_button, 0, 0)
        self.add_remove_files_buttons_layout.addWidget(self.remove_files_button, 0, 1)
        self.add_remove_files_buttons_layout.addWidget(self.add_remove_files_buttons_spacer, 0, 2)
        self.add_remove_files_buttons_layout.setColumnStretch(2, 1)
        self.central_widget.setLayout(self.central_layout)
        self.central_layout.addWidget(self.add_remove_files_buttons_frame)
        self.central_layout.addWidget(self.list_box)
        self.central_layout.addWidget(self.submit_button_frame)
        self.setCentralWidget(self.central_widget)
        self.add_files_button_icon = QIcon()
        self.remove_files_button_icon = QIcon()
        self.add_files_button_icon.addPixmap(QPixmap(resources.get_image_path(_ADD_FILES)))
        self.remove_files_button_icon.addPixmap(QPixmap(resources.get_image_path(_REMOVE_FILES)))
        self.add_files_button.setIcon(self.add_files_button_icon)
        self.remove_files_button.setIcon(self.remove_files_button_icon)
        self.list_box.setSelectionMode(QAbstractItemView.ExtendedSelection)
        self.submit_button.setText('Submit')
        self.add_files_button.clicked.connect(self.addFiles)
        self.remove_files_button.clicked.connect(self.removeFiles)
        self.submit_button.clicked.connect(self.close)
        self.files = []

    def showEvent(self, event):
        self.files = []
        while(self.list_box.count() > 0):
            item = self.list_box.item(0)
            self.list_box.takeItem(self.list_box.row(item))
            del item

    def closeEvent(self, event):
        for item_index in range(self.list_box.count()):
            self.files.append(self.list_box.item(item_index).text())
        
        settings.set(_RESOLUTION, str(self.geometry().width())+'x'+str(self.geometry().height()))
        settings.set(_POSITION, str(self.geometry().x())+'+'+str(self.geometry().y()))
            
        event.accept()

    def dragEnterEvent(self, event):
        if event.mimeData().hasUrls():
            event.accept()
        else:
            event.ignore()

    def dropEvent(self, event):
        files = [u.toLocalFile() for u in event.mimeData().urls()]
        for f in files:
            item = QListWidgetItem(f)
            self.list_box.addItem(item)

    def addFiles(self):
        filter = "all files (*.*)"
        initialdirectory = settings.get(_LAST_OPENED_DIRECTORY)
        add_files_browser_title = resources.get_text(_ADD_FILES_BROWSER_TITLE)
        dlg = QFileDialog(caption = add_files_browser_title, directory = initialdirectory, filter = filter)
        dlg.setFileMode(QFileDialog.ExistingFiles)
        if dlg.exec_():
            filepaths = dlg.selectedFiles()
        # Retrieve title of file browser from resources.xml
        # Open file browser from the location that was last used by bcells application
        # Initiliaze lastOpenedDirectory as initialdirectory i.e. the actual
        # last opened directory in case file selection was canceled
            last_opened_directory = initialdirectory
            #Insert all selected files into listView and set last_opened_directory
            
            # Save new last_opened_directory to settings

            for filepath in filepaths:
                filedirectory, _ = path.split(filepath)
                last_opened_directory = filedirectory
                item = QListWidgetItem(filepath)
                self.list_box.addItem(item)
            settings.set(setting = _LAST_OPENED_DIRECTORY, value = last_opened_directory)

    def removeFiles(self):
        for item in self.list_box.selectedItems():
            self.list_box.takeItem(self.list_box.row(item))
            del item
        
    def eventFilter(self, source, event):
        if event.type() == QEvent.KeyPress and event.key() in [Qt.Key_Backspace, Qt.Key_Delete]:
            for item in self.list_box.selectedItems():
                self.list_box.takeItem(self.list_box.row(item))
                del item
        return super().eventFilter(source, event)


def select_files_gui(app, ui):
    """
    Shows a graphical user interface to select files.
    As arguments, use the return values of initialize_gui

    Arguments
    app: an instance of QApplication
    ui: an instance of MainWidget
    Returns
    a list of filepaths as strings.
    """
    ui.show()
    app.exec_()
    files = ui.files.copy()
    return files

def select_directory_gui(context: str = None):
    """
    Shows a graphical user interface to select an output directory
    Returns:
    directory as string
    """
    initialdirectory = settings.get(_LAST_OPENED_DIRECTORY)
    #directory = str(QFileDialog.getExistingDirectory(directory = initialdirectory,caption =  "Select Directory"))
    if context is not None:
        caption = context;
    else:
        caption = 'Select directory';
    dlg = QFileDialog(caption = caption, directory = initialdirectory)
    dlg.setFileMode(QFileDialog.Directory)
    directory = ''
    if dlg.exec_():
        directory = dlg.selectedFiles()[0]
        settings.set(_LAST_OPENED_DIRECTORY, directory)
    return directory

def initialize_gui():
    """
    Initialize the gui. Creates the necessary class instances. To be run only once per kernel.
    If select_files_gui is called multiple times, initialize_gui does not need to be called multiple times, but just once before
    """
    app = QApplication(sys.argv)
    ui = MainWidget()
    return [app, ui]


