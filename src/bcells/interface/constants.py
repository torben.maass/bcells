# Third-party imports
from bidict import bidict

# Local application imports
from .resources import resources

__all__ = [
    '_ACTIVE_TAB',
    '_ADD_FILES',
    '_ADD_FILES_BROWSER_TITLE',
    '_ALIGNMENT',
    '_ALIGNMENT_METHOD',
    '_ALPHA',
    '_APPLICATION_NAME',
    '_AUTO',
    '_BARY_MEAN',
    '_BARY_MEDIAN',
    '_BCA_BOOTSTRAP',
    '_BOOTSTRAP',
    '_BOOTSTRAP_TYPES',
    '_BOOTSTRAP_SUBSAMPLES',
    '_CONSECUTIVE_PROCESSING',
    '_CONTROL_QUANTITY',
    '_CSV',
    '_DENOISED',
    '_DIMENSION_REDUCTION',
    '_DOSE_AXIS_SCALE',
    '_DOSE_RESPONSE',
    '_EST_BG',
    '_EXPERIMENT_TYPE',
    '_FIRST_FRAME',
    '_FPS',
    '_GENERAL',
    '_GENERATE_ZFACTOR',
    '_JACKKNIFE',
    '_KARCHER',
    '_KARCHER_MEAN',
    '_KARCHER_MEDIAN',
    '_LAST_OPENED_DIRECTORY',
    '_LIGHT_ON',
    '_LINEAR',
    '_LINEAR_DISCRIMINANT_ANALYSIS',
    '_LOCAL_THRESH',
    '_LOCAL_THRESH_MARKERS',
    '_LOGARITHMIC',
    '_LOGGING_QUEUE',
    '_MAX_ITERATIONS',
    '_MEAN',
    '_MEAN_VARIANCE_ESTIMATOR', 
    '_MEDIAN',
    '_MULTIPLE_GROUPS',
    '_NONE',
    '_NPY',
    '_NUM_JOBS_PROCESSING',
    '_NUM_JOBS_SEGMENTATION',
    '_OPEN_SETTINGS',
    '_OUT_PREFIX',
    '_PARALLELIZATION_PROCESSING',
    '_PARALLELIZE_PROCESSING',
    '_PARALLELIZATION_SEGMENTATION',
    '_PARALLELIZE_SEGMENTATION',
    '_PERCENTILE_BOOTSTRAP',
    '_PKL',
    '_POINTWISE_MEAN',
    '_POINTWISE_MEDIAN',
    '_PNG',
    '_POSITION',
    '_POSITION_SETTINGS',
    '_PRINCIPAL_COMPONENT_ANALYSIS',
    '_PROCEDURE',
    '_PROCESSING',
    '_QUADRATIC_DISCRIMINANT_ANALYSIS',
    '_REMOVE_FILES',
    '_REPRESENTATIVE',
    '_REPRESENTATIVE_METHOD',
    '_RESOLUTION',
    '_RESOLUTION_SETTINGS',
    '_RETURN_QUEUE',
    '_SAVE',
    '_SEG_BOUNDARY',
    '_SEG_BOUNDARY_LABELED',
    '_SEG_COLOR',
    '_SEGMENTATION',
    '_SETTINGS_WINDOW_OPEN',
    '_SUBSAMPLE_TYPES',
    '_TARGET_DIRECTORY',
    '_TESTING',
    '_TRACEBUNDLE',
    '_TRACEBUNDLE_AREA',
    '_TRACEBUNDLE_ST',
    '_TRACEBUNDLE_ST_AREA',
    '_UNIMODALIZE',
    '_UNIT',
    '_VERBOSE',
    '_WHOLE_WELL_TRACE',
    '_SEGMENTATION_FILETYPES',
    '_PROCESSING_FILETYPES',
    '_FILETYPES',
    '_OPTIONS_LISTS',
    '_OPTIONS_KEYS',
    '_OPTIONS_BIDICTS'
]


_ACTIVE_TAB = 'active_tab'
_ADD_FILES = 'add_files'
_ADD_FILES_BROWSER_TITLE = 'add_files_browser_title'
_ALIGNMENT = 'alignment' 
_ALIGNMENT_METHOD = 'alignment_method' 
_ALPHA = 'alpha'
_APPLICATION_NAME = 'application_name'
_AUTO = 'auto'
_BARY_MEAN = 'bary_mean' 
_BARY_MEDIAN = 'bary_median' 
_BCA_BOOTSTRAP = 'bca_bootstrap'
_BOOTSTRAP = 'bootstrap' 
_BOOTSTRAP_SUBSAMPLES = 'bootstrap_subsamples'
_CONSECUTIVE_PROCESSING = 'consecutive_processing'
_CONTROL_QUANTITY = 'control_quantity'
_CSV = 'csv'
_DENOISED = 'denoised'
_DIMENSION_REDUCTION = 'dimension_reduction'
_DOSE_AXIS_SCALE = 'dose_axis_scale'
_DOSE_RESPONSE = 'dose_response'
_EST_BG = 'est_bg'
_EXPERIMENT_TYPE = 'experiment_type'
_FIRST_FRAME = 'first_frame'
_FPS = 'fps' 
_GENERAL = 'general' 
_GENERATE_ZFACTOR = 'generate_zfactor'
_JACKKNIFE = 'jackknife' 
_KARCHER = 'karcher'
_KARCHER_MEAN = 'karcher_mean' 
_KARCHER_MEDIAN = 'karcher_median' 
_LAST_OPENED_DIRECTORY = 'last_opened_directory'
_LIGHT_ON = 'light_on'
_LINEAR = 'linear'
_LINEAR_DISCRIMINANT_ANALYSIS = 'linear_discriminant_analysis'
_LOCAL_THRESH = 'local_thresh'
_LOCAL_THRESH_MARKERS = 'local_thresh_markers'
_LOGARITHMIC = 'logarithmic'
_LOGGING_QUEUE = 'logging_queue'
_MAX_ITERATIONS = 'max_iterations'
_MEAN = 'mean'
_MEDIAN = 'median'
_MEAN_VARIANCE_ESTIMATOR = 'mean_variance_estimator' 
_MULTIPLE_GROUPS = 'multiple_groups'
_NONE = 'none' 
_NPY = 'npy'
_NUM_JOBS_PROCESSING = 'num_jobs_processing'
_NUM_JOBS_SEGMENTATION = 'num_jobs_segmentation'
_OPEN_SETTINGS = 'open_settings'
_OUT_PREFIX = 'out_prefix'
_PARALLELIZATION_PROCESSING = 'parallelization_processing'
_PARALLELIZE_PROCESSING = 'parallelize_processing'
_PARALLELIZATION_SEGMENTATION = 'parallelization_segmentation'
_PARALLELIZE_SEGMENTATION = 'parallelize_segmentation'
_PERCENTILE_BOOTSTRAP = 'percentile_bootstrap'
_PKL = 'pkl'
_PNG = 'png'
_POINTWISE_MEAN = 'pointwise_mean'
_POINTWISE_MEDIAN = 'pointwise_median'
_POSITION = 'position'
_POSITION_SETTINGS = 'position_settings'
_PRINCIPAL_COMPONENT_ANALYSIS = 'principal_component_analysis'
_PROCEDURE = 'procedure'
_PROCESSING = 'processing' 
_QUADRATIC_DISCRIMINANT_ANALYSIS = 'quadratic_discriminant_analysis'
_REMOVE_FILES = 'remove_files'
_REPRESENTATIVE = 'representative'
_REPRESENTATIVE_METHOD = 'representative_method'
_RESOLUTION = 'resolution'
_RESOLUTION_SETTINGS = 'resolution_settings'
_RETURN_QUEUE = 'return_queue'
_SAVE = 'save'
_SEG_BOUNDARY = 'seg_boundary'
_SEG_BOUNDARY_LABELED = 'seg_boundary_labeled'
_SEG_COLOR = 'seg_color'
_SEGMENTATION = 'segmentation' 
_SETTINGS_WINDOW_OPEN = 'settings_window_open'
_TARGET_DIRECTORY = 'target_directory'
_TESTING = 'testing'
_TRACEBUNDLE = 'tracebundle'
_TRACEBUNDLE_AREA = 'tracebundle_area'
_TRACEBUNDLE_ST = 'tracebundle_st' 
_TRACEBUNDLE_ST_AREA = 'tracebundle_st_area'
_UNIMODALIZE = 'unimodalize' 
_UNIT = 'unit' 
_VERBOSE = 'verbose' 
_WHOLE_WELL_TRACE = 'whole_well_trace'
#--

_SUBSAMPLE_TYPES = [_BOOTSTRAP, _JACKKNIFE]

_BOOTSTRAP_TYPES = [_PERCENTILE_BOOTSTRAP, _BCA_BOOTSTRAP]

_SEGMENTATION_FILETYPES = [
    ("Nikon NIS-Elements ND2", ".nd2")
]
_PROCESSING_FILETYPES = [
    ("comma seperated value list", ".csv"),
    ("numpy data", ".npy"),("pickle data", ".pkl")
]

_FILETYPES = {
    _SEGMENTATION: _SEGMENTATION_FILETYPES,
    _PROCESSING: _PROCESSING_FILETYPES
}

_OPTIONS_LISTS = { 
    # In the current state, _POINTWISE_MEAN/_POINTWISE_MEDIAN together with alignment is just the Karcher mean/Karcher median (sort of)
    # thus there wouldn't need to be a _KARCHER representative method
    # This would remove the inelegant double use of srsf in two possibily contradicting situations.
    # On stronger machines it might be useful to be able to align separately for eachsubsample though or to compare
    # unaligned traces with Karcher mean/median.
    _ALIGNMENT_METHOD:         [_NONE, _KARCHER_MEAN, _KARCHER_MEDIAN], 
    _MEAN_VARIANCE_ESTIMATOR:  [_NONE, _BCA_BOOTSTRAP, _PERCENTILE_BOOTSTRAP,    _JACKKNIFE], 
    _REPRESENTATIVE_METHOD:    [_NONE, _BARY_MEAN,    _BARY_MEDIAN,     _KARCHER, _POINTWISE_MEAN, _POINTWISE_MEDIAN],
    _EXPERIMENT_TYPE:          [_AUTO, _DOSE_RESPONSE, _MULTIPLE_GROUPS],
    _DOSE_AXIS_SCALE:          [_AUTO, _LINEAR, _LOGARITHMIC],
    _DIMENSION_REDUCTION:      [_NONE, _PRINCIPAL_COMPONENT_ANALYSIS, _LINEAR_DISCRIMINANT_ANALYSIS, _QUADRATIC_DISCRIMINANT_ANALYSIS]
} 

_OPTIONS_KEYS = [ 
    _ALIGNMENT_METHOD, 
    _MEAN_VARIANCE_ESTIMATOR, 
    _REPRESENTATIVE_METHOD ,
    _EXPERIMENT_TYPE,
    _DOSE_AXIS_SCALE
] 

_OPTIONS_BIDICTS = dict([(key, bidict([(method, resources.get_text(method)) for method in _OPTIONS_LISTS[key]])) for key in _OPTIONS_KEYS])
