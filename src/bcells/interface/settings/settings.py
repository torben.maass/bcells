### Standard library imports
from configparser import RawConfigParser
from os import path, strerror
from shutil import copy
from errno import ENOENT

settings_dictionary = {
    'position': '0+0',
    'resolution': '1100x600',
    'last_opened_directory': '',
}

default_section = 'DEFAULT'
current_section = 'CURRENT'

### Fix location of settings.ini
settings_filename = 'settings.ini'
settings_backup_filename = 'settings_backup.ini'
settings_filedirectory, _ = path.split(__file__)
settings_filepath = path.join(settings_filedirectory, settings_filename)
settings_backup_filepath = path.join(settings_filedirectory, settings_backup_filename)


def check_settings_for_integrity(settings_filepath: str, settings_dictionary: dict):
    settings_parser = RawConfigParser()
    settings_parser.read(settings_filepath)
    if settings_parser.has_section('CURRENT'):
        for key in settings_dictionary:
            if not settings_parser.has_option(current_section, key):
                return False
        return True
    return False

def create_settings(settings_filepath: str, settings_dictionary: dict):
    settings_filecontent = '[{}]\n'.format(default_section)
    for key, value in settings_dictionary.items():
        settings_filecontent = settings_filecontent + '{} = {}\n'.format(key, value)
    settings_filecontent = settings_filecontent + '\n[{}]'.format(current_section)
    with open(settings_filepath, 'w') as f:
        f.write(settings_filecontent)
    return
def open_settings():
    if (not path.exists(settings_filepath)) or not check_settings_for_integrity(settings_filepath = settings_filepath, settings_dictionary = settings_dictionary):
        print(path.exists(settings_filepath))
        print(check_settings_for_integrity(settings_filepath = settings_filepath, settings_dictionary = settings_dictionary))
        if (not path.exists(settings_backup_filepath)) or not check_settings_for_integrity(settings_filepath = settings_backup_filepath, settings_dictionary = settings_dictionary):
            create_settings(settings_filepath = settings_backup_filepath, settings_dictionary = settings_dictionary)
        copy(settings_backup_filepath, settings_filepath)
    settings_parser = RawConfigParser()
    settings_parser.read(settings_filepath)
    return settings_parser


def get(setting: str) -> str:
    try:
        if not isinstance(setting, str):
            raise TypeError('Setting must be of type str, but was of type {}.',format(type(setting)))
    except TypeError:
        return None
    else:
        settings_parser = open_settings()
        try:
            if not setting in settings_parser[default_section]:
                raise ValueError('Setting \'{}\' not found in {}.'.format(setting ,settings_filename))
        except ValueError:
            return None
        else:
            result = settings_parser[current_section][setting]
            return result
            

def set(setting: str, value: str):
    try:
        if not isinstance(setting, str):
            raise TypeError('Setting must be of type str, but was of type {}.'.format(type(setting)))
        if not isinstance(value, str):
            raise TypeError('Value must be of type str, but was of type {}.'.format(type(value)))
    except TypeError:
        return
    else:
        settings_parser = open_settings()
        try:
            if not setting in settings_parser[default_section]:
                raise ValueError('Setting \'{}\' not found in {}.'.format(setting ,settings_filename))
        except ValueError:
            return
        else:
            settings_parser[current_section][setting] = value
            with open(settings_filepath, 'w') as f:
                settings_parser.write(f)
            return
    
def reset():
    settings_parser = open_settings()
    for setting in settings_parser[default_section]:
        settings_parser[current_section][setting] = settings_parser[default_section][setting]
    with open(settings_filepath, 'w') as f:
        settings_parser.write(f)
    return
