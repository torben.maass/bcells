### Standard library imports
from os import path
import xml.etree.ElementTree as ET
### Fix the location of the resources.xml
resources_filedirectory, _ = path.split(__file__)
resources_filename = 'resources.xml'
resources_filepath = path.join(resources_filedirectory, resources_filename)

def get_image_path(image_name: str) -> str:
    tree = ET.parse(resources_filepath)
    root = tree.getroot()
    images = root.find('images')

    image = images.find(image_name)

    images_directory = images.find('path').text
    image_path = image.find('path').text
    return(path.join(path.join(resources_filedirectory,images_directory),image_path))

def get_text(text_name: str) -> str:
    tree = ET.parse(resources_filepath)
    root = tree.getroot()
    texts = root.find('texts')
    text = texts.find(text_name)
    text_content = text.text
    return(text_content)

def get_color(color_name: str) -> str:
    tree = ET.parse(resources_filepath)
    root = tree.getroot()
    colors = root.find('colors')
    color = colors.find(color_name)
    return(color.text)