from os import path
import pickle

def get_example_traces():
    """Fetches the traces from the data directory.

    Returns:
        dict: A dictionary of the traces.
    """
    path_to_folder = '.'
    file_0 = '../src/bcells/data/traces_bg_0muM_subset.pkl'
    file_100 = '../src/bcells/data/traces_bg_100muM_subset.pkl'  
    path_to_file_0 = path.join(path_to_folder, file_0)
    path_to_file_100 = path.join(path_to_folder, file_100)

    with open(path_to_file_0, "rb") as f:
        dat_0 = pickle.load(f)
    with open(path_to_file_100, "rb") as f:
        dat_100 = pickle.load(f)
    return [dat_0, dat_100], ['traces_bg_0muM_subset.pkl', 'traces_bg_100muM_subset.pkl']