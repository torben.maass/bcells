"""bcells - Brightness-based cell segmentation and analysis

This package provides a set of tools for the segmentation and analysis of
image sequence data.

The package is divided into the following submodules:
    - processing: Functions for image processing and analysis
    - segmentation: Functions for cell segmentation
    - uni: Functions for the analysis of univariate data
    - util: Utility functions
    - data: Example data
"""

from bcells import (
    interface, processing, segmentation, uni, util
)
